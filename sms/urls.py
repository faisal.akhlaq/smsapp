from django.conf.urls import url
from . import views
#from django.conf.urls import patterns, url

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'send_sms_view/', views.send_sms_view, name='send_sms_view'),
    url(r'sent_messages/', views.sent_messages, name='sent_messages'),
    url(r'message_detail/', views.message_detail, name='message_detail'),
]

