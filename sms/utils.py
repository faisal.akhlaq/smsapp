import nexmo
from .models import SmsInfo
from .models import Sms
from django.conf import settings

"""
save_sms_info is getting the response parameter from send_sms_using_nexmo().
we will extract sms information from response and save it in the model
"""
def save_sms_info(res, sms):
    smsinfo = SmsInfo()
    
#directly accesing class variable(v.bad)
#TODO read about class private
#TODO display meaningful exception message 

    response = res['messages'][0]
    try:
        smsinfo.sender = sms.sender_name
    except Exception as e:
        print("Value not found in response")
        #print ('%s (%s)') % (e.message(), type(e))

    try:
        smsinfo.receiver = response['to']
    except Exception as e:
        print("Value not found in response")

    try:
        text = sms.title +" \n "+ sms.text
        smsinfo.message_text = text
    except Exception as e:
        print("Value not found in response")

    try:
        smsinfo.price = response['message-price']
    except Exception as e:
        print("Value not found in response")

    try:
        smsinfo.message_count = res['message-count']
    except Exception as e:
        print("Value not found in response")

    try:
        smsinfo.status = response['status']
    except Exception as e:
        print("Value not found in response")

    try:
        smsinfo.timestamp = sms.date_sent
    except Exception as e:
        print("Value not found in response")

    smsinfo.save()

#def send_sms_using_nexmo(frm=None, to=None, text=None):
def send_sms_using_nexmo(form):
    """Shortcut to send a sms using libnexmo api.
    :param frm: The originator of the message
    :param to: The message's recipient
    :param text: The text message body
    Example usage:
    >>> send_message(to='+33123456789', text='My sms message body')
    """

    txt = form.text
    titl = form.title
    to = form.receiver
    text_to_send = titl +"\n "+ txt 

    assert to is not None
    assert txt is not None

#    if frm is None:
    frm = settings.NEXMO_DEFAULT_FROM

    client = nexmo.Client(key=settings.NEXMO_API_KEY, secret=settings.NEXMO_API_SECRET)
    response = client.send_message({
        'from': frm,
        'to': to,
        'text': text_to_send
    })
    return response
