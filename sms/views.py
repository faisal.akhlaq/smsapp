from django.shortcuts import render
from .models import Sms
from .forms import SendSmsForm
from django.shortcuts import redirect
from django.utils import timezone
from .utils import send_sms_using_nexmo
from .utils import save_sms_info

def home(request):
    return render(request, 'sms/home.html',{})

def sent_messages(request):
    messages = Sms.objects.all().order_by('date_sent')
    return render(request, 'sms/sent_messages.html', {'messages': messages})

def message_detail(request):
    response = {'message-count':1, 'message':{'status':0, 'message-price':0.00092, 'to':4571977799 }}
    return render(request, 'sms/message_detail.html', {'response': response})

def send_sms_view(request):
    if request.method == "POST":
        form = SendSmsForm(request.POST)
        if form.is_valid():
            sms = form.save(commit=False)
            sms.date_sent = timezone.now()
            sms.save()

            res = send_sms_using_nexmo(sms)
            response = res['messages'][0]
            
            if (response['status'] == '0'):

                #save the data in the model
                save_sms_info(res, sms)
                return render(request, 'sms/message_detail.html', {'response': response})
            else:
                #TODO display the error message received from nexmo of the same page
                print('Error:', response['error-text'])

    else:
        form = SendSmsForm()
    return render(request, 'sms/send_sms_view.html', {'form': form})
