from django.db import models
from django.utils import timezone

class Sms(models.Model):
    #name of the sender to be displayed to the receiver
    sender_name = models.CharField(max_length=200)
    #receiver field stores the receivers number. What if number starts with zero 0???
    receiver = models.CharField(max_length=12) 
    #title of the sms
    title = models.CharField(max_length=200)
    text = models.TextField()
    date_sent = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "Title: " + self.title + ", Text: " + self.text


##################################################################


class SmsInfo(models.Model):
#TODO read about class private/public variable. create get/set methods for class variables
#TODO implement a try catch 
    sender = models.CharField(max_length=100)
    receiver = models.CharField(max_length=12)
    #sending same message to many customers. so saving text is not very smart 
    message_text = models.TextField()
    price = models.FloatField()
    #messages longer then a length are counted as more then one. therefore we save the number of messages sent in one sms under message-count
    message_count = models.IntegerField()
    #if it was delivered or some other error occured
    status = models.CharField(max_length=2)
    #the time message was sent
    timestamp = models.DateTimeField(default=timezone.now)

